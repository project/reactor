CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------
This is [www.reactor.am](https://www.reactor.am/) integration for **drupal**.

This module adds [www.reactor.am](https://www.reactor.am/) tracking and 
communication service to your drupal website.

Reactor.am is the most important from marketing automation, CRM and user 
analytics, in one easy to use service.


REQUIREMENTS
------------
No special requirements.


RECOMMENDED MODULES
-------------------
 * Markdown filter (https://www.drupal.org/project/markdown):
   When enabled, display of the project's README.md help will be rendered
   with markdown.


INSTALLATION
------------
Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
 * Register at [www.reactor.am](https://www.reactor.am/)
 * Find your *Application ID* in your profile 
	[settings](http://www.reactor.am/settings/profile/)
 * Enter the *Application ID* into drupal module settings
 * You are now collecting your user's data into your reactor account. Define 
communication rules, segments and actions.
	

MAINTAINERS
-----------
 * Martin Šmid (smido) - https://www.drupal.org/user/3167837/
