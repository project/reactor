<?php

/**
 * @file
 * Administrative page callbacks for the analyticsjs module.
 */

/**
 * System settings form for admin.
 */
function reactor_admin_settings_form($form_state) {
  $form['account'] = array(
    '#type' => 'fieldset',
    '#title' => t('Basic Settings'),
  );

  $form['account']['reactor_app_id'] = array(
    '#title' => t('Application ID'),
    '#type' => 'textfield',
    '#default_value' => variable_get('reactor_app_id', ''),
    '#size' => 40,
    '#maxlength' => 36,
    '#required' => TRUE,
    '#description' => t('This Application ID is unique to each <a href="@reactor">Reactor.am</a> clients. To get an Application ID, <a href="@register">register your Project with Reactor.am</a>, or if you already have registered your site, go to your Reactor.am Settings page. Reactor\'s javascript will use this Application ID to send data to your project.', array('@reactor' => 'https://www.reactor.am/login', '@register' => 'https://www.reactor.am/login')),
  );

  // Advanced reactor configurations.
  $form['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advance Configurations'),
  );

  // Privacy configurations.
  $form['advanced']['reactor_privacy'] = array(
    '#type' => 'checkbox',
    '#title' => t('Universal web tracking opt-out'),
    '#description' => t('If enabled and your server receives the <a href="@donottrack">Do-Not-Track</a> header from the client browser, the reactor module will not embed any tracking code into your site. Compliance with Do Not Track could be purely voluntary, enforced by industry self-regulation, or mandated by state or federal law. Please accept your visitors privacy. If they have opt-out from tracking and advertising, you should accept their personal decision. This feature is currently limited to logged in users and disabled page caching.', array('@donottrack' => 'http://donottrack.us/')),
    '#default_value' => variable_get('reactor_privacy', 1),
  );

  return system_settings_form($form);
}
